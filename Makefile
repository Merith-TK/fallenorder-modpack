default:
	@echo "No Default make command configured"
	@echo "Please use either"
	@echo "   - make multimc"
	@echo "or"
	@echo "   - make technic"
	@echo ""
	@echo "MultiMC will make a pack with the updater.exe built in"
	@echo "  along with the git history"
	@echo ""
	@echo "Techic will make a pack that can be used for technic"

multimc:
	-rm  -rf update-pack.data/temp
	-rm  -rf update-pack.data/bin
	-rm  -rf update-pack.data/busybox.exe
	7z d ../fallen-order.zip ./* -r
	7z d ../fallen-order.zip ./.* -r
	7z a ../fallen-order.zip ./* -r
	7z a ../fallen-order.zip ./.git -r
	7z a ../fallen-order.zip ./.minecraft -r

technic:
	7z d ../fallen-order-technic.zip ./* -r
	7z a ../fallen-order-technic.zip ./.minecraft/* -r
	7z a ../fallen-order-technic.zip ./icon.png

all: multimc technic